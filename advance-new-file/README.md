# Geany AdvanceNewFile (BETA)

> File creation plugin for Geany

![screenshot](screenshot.png "AdvanceNewFile")

### Requirements

[Geanylua](https://github.com/geany/geany-plugins/tree/master/geanylua)

### License

GPL-3 <https://www.gnu.org/licenses/gpl-3.0.html>

### Note

Based on AdvanceNewFile from SublimeText

#### Version

2.5
